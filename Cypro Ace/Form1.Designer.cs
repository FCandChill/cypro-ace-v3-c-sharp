﻿namespace CyproAce
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveToDiskToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SpreadsheetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.importFuzzyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OtherToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.findAndReplaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.WriteAndLaunchGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.RichTextBoxOriginal = new System.Windows.Forms.TextBox();
            this.SplitContainer_main = new System.Windows.Forms.SplitContainer();
            this.splitContainer2_left_side_pane = new System.Windows.Forms.SplitContainer();
            this.groupBox_select_entry = new System.Windows.Forms.GroupBox();
            this.splitContainer_select_entry = new System.Windows.Forms.SplitContainer();
            this.ComboBoxEntry = new System.Windows.Forms.ComboBox();
            this.groupBox_search = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButton_menu = new System.Windows.Forms.RadioButton();
            this.radioButton_new = new System.Windows.Forms.RadioButton();
            this.radioButton_original = new System.Windows.Forms.RadioButton();
            this.radioButton_Comment = new System.Windows.Forms.RadioButton();
            this.checkBox_regex = new System.Windows.Forms.CheckBox();
            this.CheckBoxProofread = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.textBox_search = new System.Windows.Forms.TextBox();
            this.button_search = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.button_edit_name = new System.Windows.Forms.Button();
            this.TextBoxName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ListBoxMenu = new System.Windows.Forms.ListBox();
            this.button_save_to_RAM = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.splitContainer_script = new System.Windows.Forms.SplitContainer();
            this.groupBox_original = new System.Windows.Forms.GroupBox();
            this.SplitContainerOriginal = new System.Windows.Forms.SplitContainer();
            this.groupBox_preview_original = new System.Windows.Forms.GroupBox();
            this.RichTextBoxPreviewOriginal = new System.Windows.Forms.TextBox();
            this.SplitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox_new = new System.Windows.Forms.GroupBox();
            this.SplitContainerNew = new System.Windows.Forms.SplitContainer();
            this.elementHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.groupBox_preview_new = new System.Windows.Forms.GroupBox();
            this.RichTextBoxPreviewNew = new System.Windows.Forms.RichTextBox();
            this.groupBox_proof = new System.Windows.Forms.GroupBox();
            this.SplitContainerProof = new System.Windows.Forms.SplitContainer();
            this.ElementHost2 = new System.Windows.Forms.Integration.ElementHost();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.RichTextBoxPreviewProof = new System.Windows.Forms.RichTextBox();
            this.groupBox_comment = new System.Windows.Forms.GroupBox();
            this.RichTextBoxComment = new System.Windows.Forms.TextBox();
            this.bindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSource2 = new System.Windows.Forms.BindingSource(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressbar = new System.Windows.Forms.ToolStripProgressBar();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer_main)).BeginInit();
            this.SplitContainer_main.Panel1.SuspendLayout();
            this.SplitContainer_main.Panel2.SuspendLayout();
            this.SplitContainer_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2_left_side_pane)).BeginInit();
            this.splitContainer2_left_side_pane.Panel1.SuspendLayout();
            this.splitContainer2_left_side_pane.Panel2.SuspendLayout();
            this.splitContainer2_left_side_pane.SuspendLayout();
            this.groupBox_select_entry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_select_entry)).BeginInit();
            this.splitContainer_select_entry.Panel1.SuspendLayout();
            this.splitContainer_select_entry.Panel2.SuspendLayout();
            this.splitContainer_select_entry.SuspendLayout();
            this.groupBox_search.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_script)).BeginInit();
            this.splitContainer_script.Panel1.SuspendLayout();
            this.splitContainer_script.Panel2.SuspendLayout();
            this.splitContainer_script.SuspendLayout();
            this.groupBox_original.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerOriginal)).BeginInit();
            this.SplitContainerOriginal.Panel1.SuspendLayout();
            this.SplitContainerOriginal.Panel2.SuspendLayout();
            this.SplitContainerOriginal.SuspendLayout();
            this.groupBox_preview_original.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).BeginInit();
            this.SplitContainer2.Panel1.SuspendLayout();
            this.SplitContainer2.Panel2.SuspendLayout();
            this.SplitContainer2.SuspendLayout();
            this.groupBox_new.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerNew)).BeginInit();
            this.SplitContainerNew.Panel1.SuspendLayout();
            this.SplitContainerNew.Panel2.SuspendLayout();
            this.SplitContainerNew.SuspendLayout();
            this.groupBox_preview_new.SuspendLayout();
            this.groupBox_proof.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerProof)).BeginInit();
            this.SplitContainerProof.Panel1.SuspendLayout();
            this.SplitContainerProof.Panel2.SuspendLayout();
            this.SplitContainerProof.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox_comment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.SpreadsheetToolStripMenuItem,
            this.OtherToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(896, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.SaveToDiskToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.loadToolStripMenuItem.Text = "Load Project";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.LoadToolStripMenuItem_Click);
            // 
            // SaveToDiskToolStripMenuItem
            // 
            this.SaveToDiskToolStripMenuItem.Enabled = false;
            this.SaveToDiskToolStripMenuItem.Name = "SaveToDiskToolStripMenuItem";
            this.SaveToDiskToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.SaveToDiskToolStripMenuItem.Text = "Save to Disk";
            this.SaveToDiskToolStripMenuItem.Click += new System.EventHandler(this.SaveToDiskToolStripMenuItem_Click);
            // 
            // SpreadsheetToolStripMenuItem
            // 
            this.SpreadsheetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.importToolStripMenuItem,
            this.importFuzzyToolStripMenuItem});
            this.SpreadsheetToolStripMenuItem.Enabled = false;
            this.SpreadsheetToolStripMenuItem.Name = "SpreadsheetToolStripMenuItem";
            this.SpreadsheetToolStripMenuItem.Size = new System.Drawing.Size(83, 20);
            this.SpreadsheetToolStripMenuItem.Text = "Spreadsheet";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.exportToolStripMenuItem.Text = "Export";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.ExportToolStripMenuItem_Click);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.importToolStripMenuItem.Text = "Import (Exact)";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.ImportExactToolStripMenuItem_Click);
            // 
            // importFuzzyToolStripMenuItem
            // 
            this.importFuzzyToolStripMenuItem.Name = "importFuzzyToolStripMenuItem";
            this.importFuzzyToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.importFuzzyToolStripMenuItem.Text = "Import (Fuzzy)";
            this.importFuzzyToolStripMenuItem.Click += new System.EventHandler(this.ImportFuzzyToolStripMenuItem_Click);
            // 
            // OtherToolStripMenuItem
            // 
            this.OtherToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findAndReplaceToolStripMenuItem,
            this.WriteAndLaunchGameToolStripMenuItem});
            this.OtherToolStripMenuItem.Enabled = false;
            this.OtherToolStripMenuItem.Name = "OtherToolStripMenuItem";
            this.OtherToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.OtherToolStripMenuItem.Text = "Other";
            // 
            // findAndReplaceToolStripMenuItem
            // 
            this.findAndReplaceToolStripMenuItem.Name = "findAndReplaceToolStripMenuItem";
            this.findAndReplaceToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.findAndReplaceToolStripMenuItem.Text = "Find and Replace";
            this.findAndReplaceToolStripMenuItem.Click += new System.EventHandler(this.FindAndReplaceToolStripMenuItem_Click);
            // 
            // WriteAndLaunchGameToolStripMenuItem
            // 
            this.WriteAndLaunchGameToolStripMenuItem.Name = "WriteAndLaunchGameToolStripMenuItem";
            this.WriteAndLaunchGameToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.WriteAndLaunchGameToolStripMenuItem.Text = "Run Write Script";
            this.WriteAndLaunchGameToolStripMenuItem.Click += new System.EventHandler(this.RunWriteScriptToolStripMenuItem_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // RichTextBoxOriginal
            // 
            this.RichTextBoxOriginal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextBoxOriginal.Font = new System.Drawing.Font("Courier New", 10F);
            this.RichTextBoxOriginal.Location = new System.Drawing.Point(0, 0);
            this.RichTextBoxOriginal.Multiline = true;
            this.RichTextBoxOriginal.Name = "RichTextBoxOriginal";
            this.RichTextBoxOriginal.ReadOnly = true;
            this.RichTextBoxOriginal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RichTextBoxOriginal.Size = new System.Drawing.Size(179, 101);
            this.RichTextBoxOriginal.TabIndex = 0;
            // 
            // SplitContainer_main
            // 
            this.SplitContainer_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer_main.Enabled = false;
            this.SplitContainer_main.Location = new System.Drawing.Point(0, 24);
            this.SplitContainer_main.Name = "SplitContainer_main";
            // 
            // SplitContainer_main.Panel1
            // 
            this.SplitContainer_main.Panel1.Controls.Add(this.splitContainer2_left_side_pane);
            // 
            // SplitContainer_main.Panel2
            // 
            this.SplitContainer_main.Panel2.Controls.Add(this.splitContainer1);
            this.SplitContainer_main.Size = new System.Drawing.Size(896, 484);
            this.SplitContainer_main.SplitterDistance = 281;
            this.SplitContainer_main.TabIndex = 6;
            // 
            // splitContainer2_left_side_pane
            // 
            this.splitContainer2_left_side_pane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2_left_side_pane.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2_left_side_pane.Name = "splitContainer2_left_side_pane";
            this.splitContainer2_left_side_pane.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2_left_side_pane.Panel1
            // 
            this.splitContainer2_left_side_pane.Panel1.Controls.Add(this.groupBox_select_entry);
            // 
            // splitContainer2_left_side_pane.Panel2
            // 
            this.splitContainer2_left_side_pane.Panel2.Controls.Add(this.button_save_to_RAM);
            this.splitContainer2_left_side_pane.Size = new System.Drawing.Size(281, 484);
            this.splitContainer2_left_side_pane.SplitterDistance = 382;
            this.splitContainer2_left_side_pane.TabIndex = 2;
            // 
            // groupBox_select_entry
            // 
            this.groupBox_select_entry.Controls.Add(this.splitContainer_select_entry);
            this.groupBox_select_entry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_select_entry.Location = new System.Drawing.Point(0, 0);
            this.groupBox_select_entry.Name = "groupBox_select_entry";
            this.groupBox_select_entry.Size = new System.Drawing.Size(281, 382);
            this.groupBox_select_entry.TabIndex = 1;
            this.groupBox_select_entry.TabStop = false;
            this.groupBox_select_entry.Text = "Select Entry";
            // 
            // splitContainer_select_entry
            // 
            this.splitContainer_select_entry.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_select_entry.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_select_entry.Name = "splitContainer_select_entry";
            this.splitContainer_select_entry.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer_select_entry.Panel1
            // 
            this.splitContainer_select_entry.Panel1.Controls.Add(this.ComboBoxEntry);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.groupBox_search);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.label3);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.button_edit_name);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.TextBoxName);
            this.splitContainer_select_entry.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer_select_entry.Panel2
            // 
            this.splitContainer_select_entry.Panel2.Controls.Add(this.ListBoxMenu);
            this.splitContainer_select_entry.Size = new System.Drawing.Size(275, 363);
            this.splitContainer_select_entry.SplitterDistance = 175;
            this.splitContainer_select_entry.TabIndex = 1;
            // 
            // ComboBoxEntry
            // 
            this.ComboBoxEntry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxEntry.FormattingEnabled = true;
            this.ComboBoxEntry.Location = new System.Drawing.Point(59, 39);
            this.ComboBoxEntry.Name = "ComboBoxEntry";
            this.ComboBoxEntry.Size = new System.Drawing.Size(181, 21);
            this.ComboBoxEntry.TabIndex = 15;
            this.ComboBoxEntry.SelectedIndexChanged += new System.EventHandler(this.ComboBoxEntry_SelectedIndexChanged);
            // 
            // groupBox_search
            // 
            this.groupBox_search.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox_search.Controls.Add(this.flowLayoutPanel1);
            this.groupBox_search.Location = new System.Drawing.Point(6, 66);
            this.groupBox_search.Name = "groupBox_search";
            this.groupBox_search.Size = new System.Drawing.Size(266, 107);
            this.groupBox_search.TabIndex = 14;
            this.groupBox_search.TabStop = false;
            this.groupBox_search.Text = "Search";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.radioButton_menu);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_new);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_original);
            this.flowLayoutPanel1.Controls.Add(this.radioButton_Comment);
            this.flowLayoutPanel1.Controls.Add(this.checkBox_regex);
            this.flowLayoutPanel1.Controls.Add(this.CheckBoxProofread);
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(260, 88);
            this.flowLayoutPanel1.TabIndex = 19;
            // 
            // radioButton_menu
            // 
            this.radioButton_menu.AutoSize = true;
            this.radioButton_menu.Checked = true;
            this.radioButton_menu.Location = new System.Drawing.Point(3, 3);
            this.radioButton_menu.Name = "radioButton_menu";
            this.radioButton_menu.Size = new System.Drawing.Size(52, 17);
            this.radioButton_menu.TabIndex = 11;
            this.radioButton_menu.TabStop = true;
            this.radioButton_menu.Text = "Menu";
            this.radioButton_menu.UseVisualStyleBackColor = true;
            // 
            // radioButton_new
            // 
            this.radioButton_new.AutoSize = true;
            this.radioButton_new.Location = new System.Drawing.Point(61, 3);
            this.radioButton_new.Name = "radioButton_new";
            this.radioButton_new.Size = new System.Drawing.Size(47, 17);
            this.radioButton_new.TabIndex = 13;
            this.radioButton_new.Text = "New";
            this.radioButton_new.UseVisualStyleBackColor = true;
            // 
            // radioButton_original
            // 
            this.radioButton_original.AutoSize = true;
            this.radioButton_original.Location = new System.Drawing.Point(114, 3);
            this.radioButton_original.Name = "radioButton_original";
            this.radioButton_original.Size = new System.Drawing.Size(60, 17);
            this.radioButton_original.TabIndex = 12;
            this.radioButton_original.Text = "Original";
            this.radioButton_original.UseVisualStyleBackColor = true;
            // 
            // radioButton_Comment
            // 
            this.radioButton_Comment.AutoSize = true;
            this.radioButton_Comment.Location = new System.Drawing.Point(180, 3);
            this.radioButton_Comment.Name = "radioButton_Comment";
            this.radioButton_Comment.Size = new System.Drawing.Size(69, 17);
            this.radioButton_Comment.TabIndex = 18;
            this.radioButton_Comment.Text = "Comment";
            this.radioButton_Comment.UseVisualStyleBackColor = true;
            // 
            // checkBox_regex
            // 
            this.checkBox_regex.AutoSize = true;
            this.checkBox_regex.Location = new System.Drawing.Point(3, 26);
            this.checkBox_regex.Name = "checkBox_regex";
            this.checkBox_regex.Size = new System.Drawing.Size(79, 17);
            this.checkBox_regex.TabIndex = 17;
            this.checkBox_regex.Text = "Use Regex";
            this.checkBox_regex.UseVisualStyleBackColor = true;
            // 
            // CheckBoxProofread
            // 
            this.CheckBoxProofread.AutoSize = true;
            this.CheckBoxProofread.Location = new System.Drawing.Point(88, 26);
            this.CheckBoxProofread.Name = "CheckBoxProofread";
            this.CheckBoxProofread.Size = new System.Drawing.Size(72, 17);
            this.CheckBoxProofread.TabIndex = 20;
            this.CheckBoxProofread.Text = "Proofread";
            this.CheckBoxProofread.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.flowLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(3, 49);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 31);
            this.panel1.TabIndex = 19;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.textBox_search);
            this.flowLayoutPanel2.Controls.Add(this.button_search);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(210, 31);
            this.flowLayoutPanel2.TabIndex = 16;
            // 
            // textBox_search
            // 
            this.textBox_search.Location = new System.Drawing.Point(3, 3);
            this.textBox_search.Name = "textBox_search";
            this.textBox_search.Size = new System.Drawing.Size(122, 20);
            this.textBox_search.TabIndex = 10;
            // 
            // button_search
            // 
            this.button_search.Location = new System.Drawing.Point(131, 3);
            this.button_search.Name = "button_search";
            this.button_search.Size = new System.Drawing.Size(75, 23);
            this.button_search.TabIndex = 15;
            this.button_search.Text = "Search";
            this.button_search.UseVisualStyleBackColor = true;
            this.button_search.Click += new System.EventHandler(this.FilterSearch);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Entry:";
            // 
            // button_edit_name
            // 
            this.button_edit_name.Location = new System.Drawing.Point(165, 11);
            this.button_edit_name.Name = "button_edit_name";
            this.button_edit_name.Size = new System.Drawing.Size(75, 23);
            this.button_edit_name.TabIndex = 4;
            this.button_edit_name.Text = "Edit";
            this.button_edit_name.UseVisualStyleBackColor = true;
            this.button_edit_name.Click += new System.EventHandler(this.ButtonEditNameClick);
            // 
            // TextBoxName
            // 
            this.TextBoxName.Location = new System.Drawing.Point(59, 13);
            this.TextBoxName.Name = "TextBoxName";
            this.TextBoxName.Size = new System.Drawing.Size(100, 20);
            this.TextBoxName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Name:";
            // 
            // ListBoxMenu
            // 
            this.ListBoxMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListBoxMenu.FormattingEnabled = true;
            this.ListBoxMenu.Location = new System.Drawing.Point(0, 0);
            this.ListBoxMenu.Name = "ListBoxMenu";
            this.ListBoxMenu.Size = new System.Drawing.Size(275, 184);
            this.ListBoxMenu.TabIndex = 0;
            this.ListBoxMenu.SelectedIndexChanged += new System.EventHandler(this.ListBox_menu_SelectedIndexChanged);
            // 
            // button_save_to_RAM
            // 
            this.button_save_to_RAM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_save_to_RAM.Location = new System.Drawing.Point(0, 0);
            this.button_save_to_RAM.Name = "button_save_to_RAM";
            this.button_save_to_RAM.Size = new System.Drawing.Size(281, 98);
            this.button_save_to_RAM.TabIndex = 15;
            this.button_save_to_RAM.Text = "Save to RAM";
            this.button_save_to_RAM.UseVisualStyleBackColor = true;
            this.button_save_to_RAM.Click += new System.EventHandler(this.ButtonSaveToRAMClick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.groupBox_comment);
            this.splitContainer1.Size = new System.Drawing.Size(611, 484);
            this.splitContainer1.SplitterDistance = 253;
            this.splitContainer1.TabIndex = 7;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.splitContainer_script);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(611, 253);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Script";
            // 
            // splitContainer_script
            // 
            this.splitContainer_script.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer_script.Location = new System.Drawing.Point(3, 16);
            this.splitContainer_script.Name = "splitContainer_script";
            // 
            // splitContainer_script.Panel1
            // 
            this.splitContainer_script.Panel1.Controls.Add(this.groupBox_original);
            // 
            // splitContainer_script.Panel2
            // 
            this.splitContainer_script.Panel2.Controls.Add(this.SplitContainer2);
            this.splitContainer_script.Size = new System.Drawing.Size(605, 234);
            this.splitContainer_script.SplitterDistance = 185;
            this.splitContainer_script.TabIndex = 0;
            // 
            // groupBox_original
            // 
            this.groupBox_original.Controls.Add(this.SplitContainerOriginal);
            this.groupBox_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_original.Location = new System.Drawing.Point(0, 0);
            this.groupBox_original.Name = "groupBox_original";
            this.groupBox_original.Size = new System.Drawing.Size(185, 234);
            this.groupBox_original.TabIndex = 1;
            this.groupBox_original.TabStop = false;
            this.groupBox_original.Text = "Original";
            // 
            // SplitContainerOriginal
            // 
            this.SplitContainerOriginal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerOriginal.Location = new System.Drawing.Point(3, 16);
            this.SplitContainerOriginal.Name = "SplitContainerOriginal";
            this.SplitContainerOriginal.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainerOriginal.Panel1
            // 
            this.SplitContainerOriginal.Panel1.Controls.Add(this.RichTextBoxOriginal);
            // 
            // SplitContainerOriginal.Panel2
            // 
            this.SplitContainerOriginal.Panel2.Controls.Add(this.groupBox_preview_original);
            this.SplitContainerOriginal.Size = new System.Drawing.Size(179, 215);
            this.SplitContainerOriginal.SplitterDistance = 101;
            this.SplitContainerOriginal.TabIndex = 0;
            this.SplitContainerOriginal.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainerOriginal_SplitterMoved);
            // 
            // groupBox_preview_original
            // 
            this.groupBox_preview_original.Controls.Add(this.RichTextBoxPreviewOriginal);
            this.groupBox_preview_original.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_preview_original.Location = new System.Drawing.Point(0, 0);
            this.groupBox_preview_original.Name = "groupBox_preview_original";
            this.groupBox_preview_original.Size = new System.Drawing.Size(179, 110);
            this.groupBox_preview_original.TabIndex = 1;
            this.groupBox_preview_original.TabStop = false;
            this.groupBox_preview_original.Text = "In-Game Preview";
            // 
            // RichTextBoxPreviewOriginal
            // 
            this.RichTextBoxPreviewOriginal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextBoxPreviewOriginal.Font = new System.Drawing.Font("Courier New", 10F);
            this.RichTextBoxPreviewOriginal.Location = new System.Drawing.Point(3, 16);
            this.RichTextBoxPreviewOriginal.Multiline = true;
            this.RichTextBoxPreviewOriginal.Name = "RichTextBoxPreviewOriginal";
            this.RichTextBoxPreviewOriginal.ReadOnly = true;
            this.RichTextBoxPreviewOriginal.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RichTextBoxPreviewOriginal.Size = new System.Drawing.Size(173, 91);
            this.RichTextBoxPreviewOriginal.TabIndex = 2;
            // 
            // SplitContainer2
            // 
            this.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer2.Name = "SplitContainer2";
            // 
            // SplitContainer2.Panel1
            // 
            this.SplitContainer2.Panel1.Controls.Add(this.groupBox_new);
            // 
            // SplitContainer2.Panel2
            // 
            this.SplitContainer2.Panel2.Controls.Add(this.groupBox_proof);
            this.SplitContainer2.Size = new System.Drawing.Size(416, 234);
            this.SplitContainer2.SplitterDistance = 213;
            this.SplitContainer2.TabIndex = 5;
            // 
            // groupBox_new
            // 
            this.groupBox_new.Controls.Add(this.SplitContainerNew);
            this.groupBox_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_new.Location = new System.Drawing.Point(0, 0);
            this.groupBox_new.Name = "groupBox_new";
            this.groupBox_new.Size = new System.Drawing.Size(213, 234);
            this.groupBox_new.TabIndex = 4;
            this.groupBox_new.TabStop = false;
            this.groupBox_new.Text = "New";
            // 
            // SplitContainerNew
            // 
            this.SplitContainerNew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerNew.Location = new System.Drawing.Point(3, 16);
            this.SplitContainerNew.Name = "SplitContainerNew";
            this.SplitContainerNew.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainerNew.Panel1
            // 
            this.SplitContainerNew.Panel1.Controls.Add(this.elementHost1);
            // 
            // SplitContainerNew.Panel2
            // 
            this.SplitContainerNew.Panel2.Controls.Add(this.groupBox_preview_new);
            this.SplitContainerNew.Size = new System.Drawing.Size(207, 215);
            this.SplitContainerNew.SplitterDistance = 101;
            this.SplitContainerNew.TabIndex = 0;
            this.SplitContainerNew.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainer_new_SplitterMoved);
            // 
            // elementHost1
            // 
            this.elementHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.elementHost1.Location = new System.Drawing.Point(0, 0);
            this.elementHost1.Name = "elementHost1";
            this.elementHost1.Size = new System.Drawing.Size(207, 101);
            this.elementHost1.TabIndex = 7;
            this.elementHost1.Text = "elementHost1";
            this.elementHost1.Child = null;
            // 
            // groupBox_preview_new
            // 
            this.groupBox_preview_new.Controls.Add(this.RichTextBoxPreviewNew);
            this.groupBox_preview_new.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_preview_new.Location = new System.Drawing.Point(0, 0);
            this.groupBox_preview_new.Name = "groupBox_preview_new";
            this.groupBox_preview_new.Size = new System.Drawing.Size(207, 110);
            this.groupBox_preview_new.TabIndex = 0;
            this.groupBox_preview_new.TabStop = false;
            this.groupBox_preview_new.Text = "In-Game Preview";
            // 
            // RichTextBoxPreviewNew
            // 
            this.RichTextBoxPreviewNew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextBoxPreviewNew.Font = new System.Drawing.Font("Courier New", 10F);
            this.RichTextBoxPreviewNew.Location = new System.Drawing.Point(3, 16);
            this.RichTextBoxPreviewNew.Name = "RichTextBoxPreviewNew";
            this.RichTextBoxPreviewNew.ReadOnly = true;
            this.RichTextBoxPreviewNew.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.RichTextBoxPreviewNew.Size = new System.Drawing.Size(201, 91);
            this.RichTextBoxPreviewNew.TabIndex = 0;
            this.RichTextBoxPreviewNew.Text = "";
            // 
            // groupBox_proof
            // 
            this.groupBox_proof.Controls.Add(this.SplitContainerProof);
            this.groupBox_proof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_proof.Location = new System.Drawing.Point(0, 0);
            this.groupBox_proof.Name = "groupBox_proof";
            this.groupBox_proof.Size = new System.Drawing.Size(199, 234);
            this.groupBox_proof.TabIndex = 5;
            this.groupBox_proof.TabStop = false;
            this.groupBox_proof.Text = "Proofread";
            // 
            // SplitContainerProof
            // 
            this.SplitContainerProof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainerProof.Location = new System.Drawing.Point(3, 16);
            this.SplitContainerProof.Name = "SplitContainerProof";
            this.SplitContainerProof.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // SplitContainerProof.Panel1
            // 
            this.SplitContainerProof.Panel1.Controls.Add(this.ElementHost2);
            // 
            // SplitContainerProof.Panel2
            // 
            this.SplitContainerProof.Panel2.Controls.Add(this.groupBox3);
            this.SplitContainerProof.Size = new System.Drawing.Size(193, 215);
            this.SplitContainerProof.SplitterDistance = 101;
            this.SplitContainerProof.TabIndex = 0;
            this.SplitContainerProof.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.SplitContainer_proof_SplitterMoved);
            // 
            // ElementHost2
            // 
            this.ElementHost2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ElementHost2.Location = new System.Drawing.Point(0, 0);
            this.ElementHost2.Name = "ElementHost2";
            this.ElementHost2.Size = new System.Drawing.Size(193, 101);
            this.ElementHost2.TabIndex = 7;
            this.ElementHost2.Text = "elementHost2";
            this.ElementHost2.Child = null;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.RichTextBoxPreviewProof);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(193, 110);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "In-Game Preview";
            // 
            // RichTextBoxPreviewProof
            // 
            this.RichTextBoxPreviewProof.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextBoxPreviewProof.Font = new System.Drawing.Font("Courier New", 10F);
            this.RichTextBoxPreviewProof.Location = new System.Drawing.Point(3, 16);
            this.RichTextBoxPreviewProof.Name = "RichTextBoxPreviewProof";
            this.RichTextBoxPreviewProof.ReadOnly = true;
            this.RichTextBoxPreviewProof.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedBoth;
            this.RichTextBoxPreviewProof.Size = new System.Drawing.Size(187, 91);
            this.RichTextBoxPreviewProof.TabIndex = 1;
            this.RichTextBoxPreviewProof.Text = "";
            // 
            // groupBox_comment
            // 
            this.groupBox_comment.Controls.Add(this.RichTextBoxComment);
            this.groupBox_comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_comment.Location = new System.Drawing.Point(0, 0);
            this.groupBox_comment.Name = "groupBox_comment";
            this.groupBox_comment.Size = new System.Drawing.Size(611, 227);
            this.groupBox_comment.TabIndex = 0;
            this.groupBox_comment.TabStop = false;
            this.groupBox_comment.Text = "Comment";
            // 
            // RichTextBoxComment
            // 
            this.RichTextBoxComment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RichTextBoxComment.Font = new System.Drawing.Font("Courier New", 10F);
            this.RichTextBoxComment.Location = new System.Drawing.Point(3, 16);
            this.RichTextBoxComment.Multiline = true;
            this.RichTextBoxComment.Name = "RichTextBoxComment";
            this.RichTextBoxComment.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RichTextBoxComment.Size = new System.Drawing.Size(605, 208);
            this.RichTextBoxComment.TabIndex = 4;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.progressbar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 486);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(896, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // progressbar
            // 
            this.progressbar.Name = "progressbar";
            this.progressbar.Size = new System.Drawing.Size(100, 16);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(896, 508);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.SplitContainer_main);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(500, 300);
            this.Name = "Form1";
            this.Text = "All-Purpose Script Manager: Cypro Ace";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.SplitContainer_main.Panel1.ResumeLayout(false);
            this.SplitContainer_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer_main)).EndInit();
            this.SplitContainer_main.ResumeLayout(false);
            this.splitContainer2_left_side_pane.Panel1.ResumeLayout(false);
            this.splitContainer2_left_side_pane.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2_left_side_pane)).EndInit();
            this.splitContainer2_left_side_pane.ResumeLayout(false);
            this.groupBox_select_entry.ResumeLayout(false);
            this.splitContainer_select_entry.Panel1.ResumeLayout(false);
            this.splitContainer_select_entry.Panel1.PerformLayout();
            this.splitContainer_select_entry.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_select_entry)).EndInit();
            this.splitContainer_select_entry.ResumeLayout(false);
            this.groupBox_search.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.splitContainer_script.Panel1.ResumeLayout(false);
            this.splitContainer_script.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer_script)).EndInit();
            this.splitContainer_script.ResumeLayout(false);
            this.groupBox_original.ResumeLayout(false);
            this.SplitContainerOriginal.Panel1.ResumeLayout(false);
            this.SplitContainerOriginal.Panel1.PerformLayout();
            this.SplitContainerOriginal.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerOriginal)).EndInit();
            this.SplitContainerOriginal.ResumeLayout(false);
            this.groupBox_preview_original.ResumeLayout(false);
            this.groupBox_preview_original.PerformLayout();
            this.SplitContainer2.Panel1.ResumeLayout(false);
            this.SplitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer2)).EndInit();
            this.SplitContainer2.ResumeLayout(false);
            this.groupBox_new.ResumeLayout(false);
            this.SplitContainerNew.Panel1.ResumeLayout(false);
            this.SplitContainerNew.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerNew)).EndInit();
            this.SplitContainerNew.ResumeLayout(false);
            this.groupBox_preview_new.ResumeLayout(false);
            this.groupBox_proof.ResumeLayout(false);
            this.SplitContainerProof.Panel1.ResumeLayout(false);
            this.SplitContainerProof.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainerProof)).EndInit();
            this.SplitContainerProof.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox_comment.ResumeLayout(false);
            this.groupBox_comment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSource2)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.TextBox RichTextBoxOriginal;
        private System.Windows.Forms.SplitContainer SplitContainer_main;
        private System.Windows.Forms.ListBox ListBoxMenu;
        private System.Windows.Forms.GroupBox groupBox_new;
        private System.Windows.Forms.GroupBox groupBox_original;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox_select_entry;
        private System.Windows.Forms.SplitContainer splitContainer_select_entry;
        private System.Windows.Forms.BindingSource bindingSource1;
        private System.Windows.Forms.Button button_edit_name;
        private System.Windows.Forms.TextBox TextBoxName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.SplitContainer splitContainer_script;
        private System.Windows.Forms.SplitContainer SplitContainerNew;
        private System.Windows.Forms.SplitContainer SplitContainerOriginal;
        private System.Windows.Forms.GroupBox groupBox_preview_new;
        private System.Windows.Forms.GroupBox groupBox_preview_original;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox_search;
        private System.Windows.Forms.Button button_search;
        private System.Windows.Forms.RadioButton radioButton_menu;
        private System.Windows.Forms.TextBox textBox_search;
        private System.Windows.Forms.RadioButton radioButton_new;
        private System.Windows.Forms.RadioButton radioButton_original;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button_save_to_RAM;
        private System.Windows.Forms.GroupBox groupBox_comment;
        private System.Windows.Forms.SplitContainer splitContainer2_left_side_pane;
        private System.Windows.Forms.TextBox RichTextBoxComment;
        private System.Windows.Forms.ToolStripMenuItem SaveToDiskToolStripMenuItem;
        private System.Windows.Forms.TextBox RichTextBoxPreviewOriginal;
        private System.Windows.Forms.CheckBox checkBox_regex;
        private System.Windows.Forms.RadioButton radioButton_Comment;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.BindingSource bindingSource2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ToolStripMenuItem SpreadsheetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar progressbar;
        private System.Windows.Forms.SplitContainer SplitContainer2;
        private System.Windows.Forms.GroupBox groupBox_proof;
        private System.Windows.Forms.SplitContainer SplitContainerProof;
        private System.Windows.Forms.Integration.ElementHost ElementHost2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RichTextBox RichTextBoxPreviewNew;
        private System.Windows.Forms.RichTextBox RichTextBoxPreviewProof;
        private System.Windows.Forms.CheckBox CheckBoxProofread;
        private System.Windows.Forms.ToolStripMenuItem OtherToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem findAndReplaceToolStripMenuItem;
        private System.Windows.Forms.Integration.ElementHost elementHost1;
        private System.Windows.Forms.ToolStripMenuItem WriteAndLaunchGameToolStripMenuItem;
        private System.Windows.Forms.ComboBox ComboBoxEntry;
        private System.Windows.Forms.ToolStripMenuItem importFuzzyToolStripMenuItem;
    }
}

