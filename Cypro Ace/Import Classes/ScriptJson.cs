﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Structs
{
    public class ScriptIndexTable
    {
        [JsonProperty(Required = Required.Always)]
        public Dictionary<string, Entry> IndexTable { get; set; }

        public ScriptIndexTable()
        {
            IndexTable = new Dictionary<string, Entry>();
        }

        public class Entry
        {
            [JsonConverter(typeof(HexStringPairJsonConverter)), JsonProperty(Required = Required.Always)]
            public Dictionary<long, PointerTable> PointerTable { get; set; }

            public Entry()
            {
                PointerTable = new Dictionary<long, PointerTable>();
            }
        }

        public class PointerTable
        {
            [JsonProperty(Required = Required.Always)]
            public Dictionary<int, PointerTableEntry> PointerTableEntry { get; set; }

            public PointerTable()
            {
                PointerTableEntry = new Dictionary<int, PointerTableEntry>();
            }
        }

        public class PointerTableEntry
        {
            public Dictionary<string, SubEntries> Subentries { get; set; }

            public PointerTableEntry()
            {
                Subentries = new Dictionary<string, SubEntries>();
            }
        }

        public class SubEntries
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public SubentryMetadata Metadata { get; set; }

            // Not sure if there's any way to omit empty dictionaries other than setting them to null
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public Dictionary<Enums.ScriptTypes, string> Text { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string SamePointerAs { get; set; }

            public SubEntries()
            {
                Text = new Dictionary<Enums.ScriptTypes, string>();
            }
        }

        public class SubentryMetadata
        {
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string File { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string PointerFile { get; set; }
            [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
            public string TextFile { get; set; }
            [JsonConverter(typeof(HexStringToLongJsonConverterFixedLength))]
            public long PointerLocation { get; set; }
            [JsonConverter(typeof(HexStringToLongJsonConverterFixedLength))]
            public long TextLocation { get; set; }
            [JsonConverter(typeof(HexStringToLongJsonConverterVariableLength))]
            public long ByteLength { get; set; }

            public SubentryMetadata(string PointerFile, string TextFile, long PointerLocation, long TextLocation, long ByteLength)
            {
                if (PointerFile != TextFile)
                {
                    this.PointerFile = PointerFile;
                    this.TextFile = TextFile;
                }
                else
                {
                    this.File = PointerFile;
                }

                this.PointerLocation = PointerLocation;
                this.TextLocation = TextLocation;
                this.ByteLength = ByteLength;
            }
        }

        public class SamePointerAs
        {
            public string IndexTable { get; set; }
            public long PointerTableLocation { get; set; }
            public long EntryNo { get; set; }
            public long SubEntry { get; set; }

            public SamePointerAs(string indexTable, long pointerLocation, long entryNo, long subEntryNo)
            {
                this.IndexTable = indexTable;
                this.PointerTableLocation = pointerLocation;
                this.EntryNo = entryNo;
                this.SubEntry = subEntryNo;
            }

            public bool Equals(string indexTable, long pointerTableLocation, long entryNo, long subEntryNo)
            {
                return this.IndexTable == indexTable && this.PointerTableLocation == pointerTableLocation && this.EntryNo == entryNo && this.SubEntry == subEntryNo;
            }
        }

        /// <summary>
        /// Allows for the script data to have the addresses in hexadecimal
        /// </summary>
        public sealed class HexStringPairJsonConverter : JsonConverter
        {
            public override bool CanConvert(Type objectType) => throw new NotImplementedException();
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
            {
                // This could have also sufficed but the indenting gets borked:
                // writer.WriteRawValue(JsonConvert.SerializeObject(value, Formatting.Indented));

                writer.WriteStartObject();
                foreach (KeyValuePair<long, ScriptIndexTable.PointerTable> kvp in (Dictionary<long, ScriptIndexTable.PointerTable>)value)
                {
                    writer.WritePropertyName(MyMath.DecToHex(kvp.Key, Prefix.X, 6));
                    serializer.Serialize(writer, kvp.Value);
                }

                writer.WriteEndObject();
            }
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
            {
                JObject jsonObject = JObject.Load(reader);
                Dictionary<long, ScriptIndexTable.PointerTable> NewScriptEntryList = new Dictionary<long, ScriptIndexTable.PointerTable>();

                foreach (KeyValuePair<string, ScriptIndexTable.PointerTable> kvp in jsonObject.ToObject<Dictionary<string, ScriptIndexTable.PointerTable>>())
                {
                    NewScriptEntryList.Add(MyMath.HexToDec(kvp.Key), kvp.Value);
                }

                return NewScriptEntryList;
            }
        }

        public sealed class HexStringToLongJsonConverterFixedLength : JsonConverter
        {
            public override bool CanConvert(Type objectType) => typeof(long).Equals(objectType);
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X, 6));
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (long)MyMath.HexToDec((string)reader.Value);
        }
        public sealed class HexStringToLongJsonConverterVariableLength : JsonConverter
        {
            public override bool CanConvert(Type objectType) => typeof(long).Equals(objectType);
            public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X));
            public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (long)MyMath.HexToDec((string)reader.Value);
        }
    }
}