﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

public class DisplayReplaceCollection
{
    [JsonProperty(Required = Required.Always)]
    public string Find { get; set; }
    public string Replace { get; set; }
    public string ReplaceOriginal { get; set; }
    public string ReplaceNew { get; set; }
}

public class ScriptManager
{
    public string WriteScript { get; set; }
    public List<DisplayReplaceCollection> DisplayReplace { get; set; }
}
public class Entry : ICloneable
{
    // Only used for the script output in case the user wants to know where the pointer is located
    public long MasterPointerLocations { get; set; }
    public long MasterPointerAddresses { get; set; }
    public long MasterLength { get; set; }

    public object Clone()
    {
        return MemberwiseClone();
    }
}

public class Script
{
    [JsonProperty(Required = Required.Always)]
    public string ScriptPath { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Original { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Comment { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool New { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Bytes { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Menu { get; set; }

    [JsonProperty(Required = Required.Always)]
    public bool Proof { get; set; }
}

public class Root
{
    [JsonProperty(Required = Required.Always)]
    public Dictionary<string, string> Files { get; set; }
    [JsonProperty(Required = Required.Always)]
    public Script Scripts { get; set; }
    public ScriptManager ScriptManager { get; set; }
}

public sealed class HexStringToLongJsonConverter : JsonConverter
{
    public override bool CanConvert(Type objectType) => typeof(long).Equals(objectType);
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) => writer.WriteValue(MyMath.DecToHex((long)value, Prefix.X));
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer) => (long)MyMath.HexToDec((string)reader.Value);
}