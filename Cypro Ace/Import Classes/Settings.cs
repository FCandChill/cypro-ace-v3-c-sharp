﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

public static class Setting
{
    public static Root Settings;

    public static void ReadSettingsFile(string PathOfSettingsFile)
    {
        Settings = JsonConvert.DeserializeObject<Root>(new StreamReader(PathOfSettingsFile).ReadToEnd());
    }
}