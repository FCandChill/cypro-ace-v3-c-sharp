﻿using System.Drawing;

public class Enums
{
    public enum ScriptTypes
    {
        Menu,
        Original,
        New,
        Comment,

        Proof,
        Bytes,
    }

    public enum ColumnNames
    {
        SubentryId,
        Index,
        PointerTableKey
    };

    public struct Chunk
    {
        public int startpos;
        public int length;
        public Color BackColor;
    }
}