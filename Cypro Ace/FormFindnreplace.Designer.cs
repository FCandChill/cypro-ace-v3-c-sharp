﻿namespace CyproAce
{
    partial class FormFindAndReplace
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_find = new System.Windows.Forms.Label();
            this.label_replace = new System.Windows.Forms.Label();
            this.textBox_find = new System.Windows.Forms.TextBox();
            this.textBox_replace = new System.Windows.Forms.TextBox();
            this.button_replace = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_find
            // 
            this.label_find.AutoSize = true;
            this.label_find.Location = new System.Drawing.Point(13, 13);
            this.label_find.Name = "label_find";
            this.label_find.Size = new System.Drawing.Size(30, 13);
            this.label_find.TabIndex = 0;
            this.label_find.Text = "Find:";
            // 
            // label_replace
            // 
            this.label_replace.AutoSize = true;
            this.label_replace.Location = new System.Drawing.Point(13, 40);
            this.label_replace.Name = "label_replace";
            this.label_replace.Size = new System.Drawing.Size(47, 13);
            this.label_replace.TabIndex = 1;
            this.label_replace.Text = "Replace";
            // 
            // textBox_find
            // 
            this.textBox_find.Location = new System.Drawing.Point(77, 10);
            this.textBox_find.MaxLength = 10;
            this.textBox_find.Name = "textBox_find";
            this.textBox_find.Size = new System.Drawing.Size(100, 20);
            this.textBox_find.TabIndex = 2;
            this.textBox_find.TextChanged += new System.EventHandler(this.textBoxFind_TextChanged);
            // 
            // textBox_replace
            // 
            this.textBox_replace.Location = new System.Drawing.Point(77, 36);
            this.textBox_replace.MaxLength = 10;
            this.textBox_replace.Name = "textBox_replace";
            this.textBox_replace.Size = new System.Drawing.Size(100, 20);
            this.textBox_replace.TabIndex = 3;
            // 
            // button_replace
            // 
            this.button_replace.Enabled = false;
            this.button_replace.Location = new System.Drawing.Point(204, 35);
            this.button_replace.Name = "button_replace";
            this.button_replace.Size = new System.Drawing.Size(75, 23);
            this.button_replace.TabIndex = 4;
            this.button_replace.Text = "Replace";
            this.button_replace.UseVisualStyleBackColor = true;
            this.button_replace.Click += new System.EventHandler(this.button_replace_Click);
            // 
            // Form_findnreplace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 72);
            this.Controls.Add(this.button_replace);
            this.Controls.Add(this.textBox_replace);
            this.Controls.Add(this.textBox_find);
            this.Controls.Add(this.label_replace);
            this.Controls.Add(this.label_find);
            this.Name = "Form_findnreplace";
            this.Text = "Find and Replace";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_find;
        private System.Windows.Forms.Label label_replace;
        private System.Windows.Forms.TextBox textBox_find;
        private System.Windows.Forms.TextBox textBox_replace;
        private System.Windows.Forms.Button button_replace;
    }
}