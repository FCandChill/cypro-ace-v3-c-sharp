﻿using System.Collections.Generic;

namespace Cypro_Ace.Class
{
    public class PointerInfo
    {
        public string IndexTable { get; set; }
        public long PointerTable { get; set; }
        public int PointerTableEntry { get; set; }
        public string SubentryId { get; set; }

        public PointerInfo(string IndexTable, long PointerTable, int PointerTableEntry, string SubentryId)
        {
            this.IndexTable = IndexTable;
            this.PointerTable = PointerTable;
            this.PointerTable = PointerTable;
            this.PointerTableEntry = PointerTableEntry;
            this.SubentryId = SubentryId;
        }
    }
}