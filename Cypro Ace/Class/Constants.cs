﻿using System.Collections.Generic;

namespace Cypro_Ace.Class
{
    public class Constants
    {
        public const string
            XLSX_EXPORT = "Script.xlsx",
            PREVIEW = "{0} (Preview)",
            SEPERATOR = " - ",
            NULL = "<null>";

        public const int START_ROW = 2;

        public const char
            BYTE1 = '{',
            BYTE2 = '}';

        public static readonly List<Enums.ColumnNames> ColumnNames = new List<Enums.ColumnNames>
    {
        Enums.ColumnNames.SubentryId,
        Enums.ColumnNames.Index,
        Enums.ColumnNames.PointerTableKey
    };

        public static readonly List<Enums.ColumnNames> NecessaryColumnNames = new List<Enums.ColumnNames>
    {
        Enums.ColumnNames.Index,
        Enums.ColumnNames.PointerTableKey,
    };

        public static readonly List<Enums.ScriptTypes> ScriptTypes = new List<Enums.ScriptTypes>
    {
        Enums.ScriptTypes.Menu,
        Enums.ScriptTypes.Original,
        Enums.ScriptTypes.New,
        Enums.ScriptTypes.Comment,
        Enums.ScriptTypes.Proof,
    };

        public static readonly List<Enums.ScriptTypes> ScriptTypesWithPreview = new List<Enums.ScriptTypes>
    {
        Enums.ScriptTypes.Original,
        Enums.ScriptTypes.New,
    };
    }
}